import argparse
import shutil
import os
import json
import pprint

def extract_aiv(archive, main_path):
    try:
        os.mkdir(main_path)
    except:
        shutil.rmtree(main_path)
        os.mkdir(main_path)

    extract_path = os.path.join(main_path, archive.split('.')[0])
    try:
        os.mkdir(extract_path)
    except FileExistsError:
        shutil.rmtree(extract_path)
        os.mkdir(extract_path)
    shutil.unpack_archive(archive, extract_path)

    return extract_path


def find_competency_files(directory):
    files = []
    for file in os.listdir(directory):
        if file.endswith(".json"):
            files.append(os.path.join(directory, file))
    return files


def extract_search_competencies(aiv_file):
    _aiv_file = None
    _aiv_file_data = None
    try:
        _aiv_file = open(aiv_file)
        _aiv_file_data = json.load(_aiv_file)
    except:
        print("Invalid JSON file.")
        exit()
    return _aiv_file_data.get('extra_states_competencies', []), _aiv_file_data.get('slot_mapper_competencies', [])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='File Input')
    parser.add_argument('--competencies', required=True, type=str,
                        help='json file with search competencies')
    parser.add_argument('--aiv', type=str, required=True,
                        help='aiv to search')
    args = parser.parse_args()

    ex_dir = extract_aiv(args.aiv, 'extracted')

    competency_files = find_competency_files(os.path.join(ex_dir, 'handler_data' + os.sep + 'simple'))
    broken_competencies = {"extra_states": [], "slot_mappers": []}
    extra_state_comps, slot_mapper_comps = extract_search_competencies(args.competencies)

    for idx, file in enumerate(competency_files):
        filename = file.split(os.sep)[-1]
        competency_name = filename.split(".")[0]

        if competency_name in extra_state_comps:
            _file = open(competency_files[idx], )
            file_data = json.load(_file)

            if file_data.get('extra_states', None) is None:
                broken_competencies['extra_states'].append(competency_name)
            _file.close()

        if competency_name in slot_mapper_comps:
            _file = open(competency_files[idx], )
            file_data = json.load(_file)

            if file_data.get('slot_mappers', None) is None or file_data.get('slot_mappers') == []:
                broken_competencies['slot_mappers'].append(competency_name)
            _file.close()

    print('Problematic Competencies:')
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(broken_competencies)
