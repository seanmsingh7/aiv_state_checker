**Description**

This is a simple script to check for inconsistencies in states and slot mappers in an AIV after using the merge tool.

**How To Use**

`pip install -r requirements.txt`

usage<br>

`python app.py --competencies competency.json --aiv 'path to aiv.tar.gz'`

once completed, the problematic aiv files will be printed in the terminal

**NOTE**

The json file should have this structure.

```
{
  "extra_states_competencies": [
    "comp_name1",
    "comp_name2",
    "comp_name3"
  ],
 "slot_mapper_competencies": [
    "comp_name1",
    "comp_name2",
    "comp_name3"
  ]
}
```